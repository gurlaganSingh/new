package seleniumPractice;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
public class seleniumDemo {
	
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "/Users/gurlaganbhullar/Desktop/chromedriver");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.seleniumeasy.com/test/basic-first-form-demo.html");
		
		WebElement inputBox = driver.findElement(By.id("user-message"));
		
		inputBox.sendKeys("here is nonsense");
		
		
		WebElement showMessageButton = driver.findElement(By.cssSelector("form#get-input button"));
		
		showMessageButton.click();
		
		
		Thread.sleep(2000);
		driver.close();
	}

}
