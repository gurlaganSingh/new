package seleniumPractice;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class testCase {
	WebDriver driver;

	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver", "/Users/gurlaganbhullar/Desktop/chromedriver");
	    driver = new ChromeDriver();
		
		driver.get("https://www.seleniumeasy.com/test/basic-first-form-demo.html");
	}

	@After
	public void tearDown() throws Exception {
		Thread.sleep(2000);
		driver.close();
	}
	
	@Test
	public void testSingleInputField() {
		
		//get the actual output from the screen
		//check actual output =  expected  output
		
		
		WebElement inputBox = driver.findElement(By.id("user-message"));
		
		inputBox.sendKeys("here is some nonsense");
		WebElement showMessageButton = driver.findElement(By.cssSelector("form#get-input button"));
		
		showMessageButton.click();
		
		
		
		WebElement outputBox = driver.findElement(By.id("display"));
		
		String actualOutput = outputBox.getText();
		assertEquals("here is some nonsense", actualOutput);
	}
	@Test
	public void testtwoInputField() {
	
		
		WebElement sum1 = driver.findElement(By.id("sum1"));
		sum1.sendKeys("40");
		WebElement sum2 = driver.findElement(By.id("sum2"));
		sum2.sendKeys("50");

WebElement showNumberButton1 = driver.findElement(By.cssSelector("form#gettotal button"));
		
		showNumberButton1.click();
		
		WebElement outputBox1 = driver.findElement(By.id("displayvalue"));
		
		String actualOutput1 = outputBox1.getText();
		assertEquals("90", actualOutput1);
	}

}
